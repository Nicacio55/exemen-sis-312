/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta3;

/**
 *
 * @author EVERT NICACIO PACHECO CI: 7450101
 */
public class PilaChar {
    private char[] pilaArreglo;
   private int tamanoArreglo;
   private int top;
 
   public PilaChar(int capacidad){
       tamanoArreglo = capacidad;
       pilaArreglo = new char[tamanoArreglo];
       top = -1;
   }
    
    
   public void push(char s) {
       pilaArreglo[++top] = s;
   }
    
   public char pop(){
       return pilaArreglo[top--];
   }
    
   public boolean vacio(){
       return top == -1;
   }
    
   public boolean lleno(){
       return top == tamanoArreglo - 1;
   }
}
