/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta3;

/**
 *
 * @author EVERT NICACIO PACHECO CI: 7450101
 */
public class Pregunta3 {

    private String[] pilaArreglo;
   private int tamanoArreglo;
   private int top;
 
   public Pregunta3(int capacidad){
       tamanoArreglo = capacidad;
       pilaArreglo = new String[tamanoArreglo];
       top = -1;
   }
    
    
   public void push(String s) {
       pilaArreglo[++top] = s;
   }
    
   public String pop(){
       return pilaArreglo[top--];
   }
    
   public boolean vacio(){
       return top == -1;
   }
    
   public boolean lleno(){
       return top == tamanoArreglo - 1;
   }
}
     

