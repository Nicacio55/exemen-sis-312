/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta3;
import java.io.*;
import java.util.*;
/**
 *
 * @author EVERT NICACIO PACHECO CI:7450101
 */
public class Infijo {
    private PilaChar stack;
    private String entrada;
    private String salida = "";
    public Infijo (String entrada){
        this.entrada = entrada;
        int tamano = entrada.length();
        stack = new PilaChar(tamano);
    }
     
    public String transforma() {
        for(int i = 0; i < entrada.length(); i++){
            char ch = entrada.charAt(i);
            switch(ch) {
                case '+':
                case '-':
                case '*':
                    oper(ch, 1);
                    break;
                case '(':
                    stack.push(ch);
                    break;
                case ')':
                    paren(ch);
                    break;
                default:
                    salida = salida + ch;
                    break;
            }
        }
         
        while(!stack.vacio()){
            salida = salida + stack.pop();
        }
         
        System.out.println(salida);
        return salida;
    }
    public void oper(char opEsto, int prec1){
        while(!stack.vacio()) {
            char opTop = stack.pop();
            if(opTop == '('){
                stack.push(opTop);
                break;
            }else{
                int prec2;
                if(opTop == '+' || opTop == '-' || opTop == '*')
                    prec2 = 1;
                else
                    prec2 = 2;
                if(prec2 < prec1) {
                    stack.push(opTop);
                    break;
                }else
                    salida = salida + opTop;
            }
        }
        stack.push(opEsto);
    }
     
    public void paren(char ch){
        while (!stack.vacio()) {
            char chx = stack.pop();
            if (chx == '(')
                break;
            else
                salida = salida + chx;
        }
    }
}
