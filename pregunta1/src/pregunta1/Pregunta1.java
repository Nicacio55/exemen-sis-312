/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pregunta1;

/**
 *
 * @author EVERT LUIS NICACIO PACHECO CI:7450101
 */
public class Pregunta1 {
    private int pila[];
    private int top;
    private int capacidad;
   
    public Pregunta1(int cap){
        capacidad=cap;
        pila=new int[capacidad];
        top=-1;
    }
   
    public boolean estaVacia(){
        return(top==-1);
    }
   
    public boolean estaLlena(){
        return((top+1)==capacidad);
    }
   
    public void apilar(int elemento){
        if(estaLlena()==false)
            pila[++top]=elemento;
        else
            System.out.println("Desbordamiento superior, no se puede apilar");
    }
   
    public int desapilar(){
        if(estaVacia()==false){
            return pila[top--];
        }
        else{
            System.out.println("Desbordamiento inferior, no se puede desapilar");
        }
        return -1;
    }
    
}

